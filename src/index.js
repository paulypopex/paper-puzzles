/* global atob, btoa, location */

const js = require('js')

// js.cE
/* function _adminLink (href, table, id, callback, anchor) {
  anchor = _createElement('a')
  if (id) anchor.id = table + id
  // href will always be appended with id
  anchor.href = href + id
  _setInnerHTML(anchor, table)
  if (callback) js.on(anchor, 'click', callback)
  return anchor
} */

// js.ok = gbbsUpdater.on.bind(gbbsUpdater, window, 'load')

const phrase = js.id('phrase')
const rows = js.id('rows')
const cols = js.id('cols')
const grid = js.id('grid')
const answer = js.id('answer')

const keys = {}
const reverseKeys = {}
let mode = 'simple'

const col = limit => String.fromCharCode(65 + Math.floor(Math.random() * (limit || cols.value)))
const row = () => Math.floor(Math.random() * rows.value + 1)

const table = rows => '<table><tr>' + rows.map(tr).join('') + '</tr></table>'
const tr = row => '<tr>' + row.map(td).join('') + '</tr>'
const td = val => '<td>' + val + '</td>'

const keyTable = () => {
  var letters = ['']
  for (var j = 1; j <= cols.value; j++) {
    letters[j] = String.fromCharCode(64 + j)
  }
  const tableRows = [letters]
  for (var i = 1; i <= rows.value; i++) {
    const row = ['<b>' + i + '</b>']
    for (var k = 1; k <= cols.value; k++) {
      const id = letters[k] + i
      let value = reverseKeys[id] || col(26)
      if (process.env.DEBUG) {
        if (reverseKeys[id] === value) {
          value = '<font color="red">' + value + '</font>'
          console.log({ i, k, id, keys, reverseKeys, value })
        }
      }
      row.push(value)
    }
    tableRows.push(row)
  }
  return table(tableRows)
}

const makeKey = l => {
  // if ( keys.hasOwnProperty( l )) return keys[l]; // reuse codes
  var letter
  var number
  var attempts = 0
  while ((!letter || reverseKeys.hasOwnProperty(keys[l])) && attempts < 20) {
    attempts++
    letter = col()
    number = row()
    keys[l] = letter + number
  }
  reverseKeys[keys[l]] = l
  return keys[l]
}

const build = () => {
  location.href = '#' + mode + '/' + encodeURIComponent(btoa(phrase.value))
  js.html(answer, '')
  phrase.value.toUpperCase().split('').forEach(l => {
    js.html(answer, answer.innerHTML + (l === ' ' ? '&nbsp;&nbsp;&nbsp;' : table([[makeKey(l)], [process.env.DEBUG ? l : '']])))
  })
  js.html(grid, keyTable())
}

const change = event => {
  window.scroll(0, 0)
  const hash = location.hash
  const path = hash.substr(1).split('/')
  mode = path[0] || 'simple'
  phrase.value = ''
  if (path[1]) {
    try {
      if (!event) { // first time round
        js.tag('form')[0].style.display = 'none'
      }
      phrase.value = atob(decodeURIComponent(path[1]))
    } catch (e) {
      console.error(e)
    }
  } else {
    js.tag('form')[0].style.display = 'default'
  }
  switch (path[0]) {
    case 'simple':
      build()
      break
    default:
      js.tag('form')[0].style.display = 'default'
  }
}

js.on(phrase, 'keyup', build)
js.on(rows, 'keyup', build)
js.on(cols, 'keyup', build)
js.on(window, 'hashchange', change)
change()

/* if ('serviceWorker' in navigator) {
  const name = process.env.CI_PROJECT_NAME
  const path = name ? `/${name}` : ''
  const serviceWorker = name ? `sw.min.js` : `sw.js`
  if (name) navigator.serviceWorker.register(`${path}/${serviceWorker}`, { scope: `${path}/` })
} */
